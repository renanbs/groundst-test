#include "terminal.h"

Terminal::Terminal(QWidget *parent) : QPlainTextEdit (parent), logFile (NULL)
{
	pText = new QPlainTextEdit ();
	QFont font;
	font.setFamily (QString::fromUtf8 ("Courier New"));
	font.setPointSize (10);
	pText->setFont (font);
	pText->setFrameShape (QFrame::Panel);
	pText->setLineWidth (1);
	pText->setUndoRedoEnabled (false);
	pText->setReadOnly (true);
	pText->setOverwriteMode (false);
	pText->setTextInteractionFlags (Qt::TextSelectableByMouse);
	pText->setMaximumBlockCount (10000);

	// Create Terminal history class
	termHist = new TerminalHistory ();
	// Create Terminal commands class
	termCom = new TerminalCommands (this, this);
	addTimeStamp ();

	QSettings settings (QSettings::UserScope, "groundST", "settings");
	if (settings.contains ("autoCRLF"))
	{
		setAutoCrLf (settings.value ("autoCRLF").toBool ());
		autocrlf = settings.value ("autoCRLF").toBool ();
	}
	else
	{
		settings.setValue ("autoCRLF", false);
		autocrlf = false;
	}
}

Terminal::~Terminal ()
{
}

void Terminal::backspace ()
{
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor );
	moveCursor (QTextCursor::Left, QTextCursor::MoveAnchor);
	QPoint p = getRowColumn();
	int len = p.x ();
	if (len >= stampLength)
	{
		moveCursor (QTextCursor::End, QTextCursor::KeepAnchor );
		textCursor ().removeSelectedText();
	}
	else
		moveCursor (QTextCursor::End, QTextCursor::MoveAnchor );
}

void Terminal::addToHistory ()
{
	QString str = getTypedCmd ();
	termHist->addToHistory (str);
}

void Terminal::nextOnCmdHistory ()
{
	nextCmdHist = termHist->nextOnCmdHistory ();
}

void Terminal::previousOnCmdHistory ()
{
	nextCmdHist = termHist->previousOnCmdHistory ();
}

void Terminal::updateCmdHistorydOnScreen ()
{
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor );
	QPoint p = getRowColumn();
	int len = p.x ();
	while (len > stampLength)
	{
		moveCursor (QTextCursor::Left, QTextCursor::MoveAnchor );
		p = getRowColumn();
		len = p.x();
	}

	moveCursor (QTextCursor::End, QTextCursor::KeepAnchor );
	textCursor ().removeSelectedText();

	cmdHist = nextCmdHist;
	insertPlainText (cmdHist);
	nextCmdHist.clear();
}

QString Terminal::getTypedCmd ()
{
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor);
	QPoint p = getRowColumn();
	int len = p.x ();
	while (len > stampLength)
	{
		moveCursor (QTextCursor::Left, QTextCursor::MoveAnchor);
		p = getRowColumn();
		len = p.x();
	}
	moveCursor (QTextCursor::End, QTextCursor::KeepAnchor);
	QString str = textCursor().selectedText();
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor);
	return str;
}

void Terminal::updateTypedCmd (QString str)
{
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor);
	QPoint p = getRowColumn();
	int len = p.x ();
	while (len > stampLength)
	{
		moveCursor (QTextCursor::Left, QTextCursor::MoveAnchor);
		p = getRowColumn();
		len = p.x();
	}
	moveCursor (QTextCursor::End, QTextCursor::KeepAnchor);
	QString typedCmd = textCursor().selectedText();
	typedCmd += str;
	textCursor().removeSelectedText();
	insertPlainText (typedCmd);
}

void Terminal::addTimeStamp ()
{
	QDateTime timeStamp = QDateTime::currentDateTime ();
	QPoint p = getRowColumn();
	int y = p.y ();
	int x = p.x ();
	if (y >= 1 && x > 0)
		insertPlainText ("\n");
	QString str = "[" + timeStamp.toString ("hh:mm:ss,zzz") + "]$ ";
	stampLength = str.length ();
	insertPlainText (str);
}

void Terminal::addTimeStampCR ()
{
	QDateTime timeStamp = QDateTime::currentDateTime ();
	QString str = "[" + timeStamp.toString ("hh:mm:ss,zzz") + "]$ ";
	stampLength = str.length ();
	insertPlainText (str);
}

QPoint Terminal::getRowColumn()
{
	QPoint xy;
	QTextCursor cursor = textCursor();
	xy.setX (cursor.columnNumber());
	xy.setY (cursor.blockNumber() + 1);
	return xy;
}

void Terminal::clearScreen ()
{
	clear ();
}

void Terminal::listHistory ()
{
	QStringList histList = termHist->listHistory ();
	int loop;
	QString pos;
	for (loop = 0; loop < histList.length (); loop++)
	{
		insertPlainText ("\n");
		pos = "  ";
		pos += QString::number (loop + 1);
		pos += "  " + histList.at (loop);
		insertPlainText (pos);
	}
}

// Reimplemented method to capture the log
void Terminal::insertPlainText (const QString &text)
{
	if (logFile)
	{
		logFile->write (text.toAscii());
		logFile->flush ();
	}
	QPlainTextEdit::insertPlainText (text);
}

void Terminal::saveScreen ()
{
	QString name = QFileDialog::getSaveFileName (this, tr ("Save Screen"), QDir::currentPath(), tr ("All Files (*)"));
	QFile file (name);
	if (!file.open (QIODevice::WriteOnly))
	{
		QString str (tr ("Cannot write file "));
		str += name;
		QMessageBox::critical (this,tr ("File read error"), str);
		return;
	}
	file.write (toPlainText().toAscii());
	file.close();
}

void Terminal::startLogging ()
{
	if (logFile)
	{
		QMessageBox::critical (this, tr ("Error"), tr("Logging already active"));
		return;
	}

	QString name = QFileDialog::getSaveFileName (this, tr ("Select log file"), QDir::currentPath (), tr ("All Files (*)"));
	if (name.length () == 0)
		return;
	logFile = new QFile (name);
	if (!logFile->open (QIODevice::WriteOnly))
	{
		QString str (tr ("Cannot write file "));
		str += name;
		QMessageBox::critical (this, tr ("File read error"), str);
		logFile = NULL;
		return;
	}
}

void Terminal::endLogging ()
{
	if (logFile)
	{
		logFile->close ();
		delete logFile;
		logFile = NULL;
	}
}

bool Terminal::parseCommand (QString str)
{
	return termCom->parseCommand (str);
}

void Terminal::help ()
{
	QString str;
	str = tr ("\ngST list of commands\n");
	str += tr ("\n");
	str += tr ("version			display gST version\n");
	str += tr ("help			print this list\n");
	str += tr ("clear			clear gST terminal\n");
	str += tr ("history			print commands history\n");
	str += tr ("exit			exit gST\n");
	insertPlainText (str);
}

void Terminal::paste ()
{
	QPlainTextEdit::paste ();
}

void Terminal::selectAll ()
{
	QPlainTextEdit::selectAll ();
}

void Terminal::copy ()
{
	QPlainTextEdit::copy ();
}

bool Terminal::treatBeforeSend (QString &str)
{
	if (autocrlf)
	{
		int loop, out_size = 0;;
		QString out_buffer;
		bool cr_received = false;
		for (loop = 0; loop < str.length (); loop++)
		{
			if (str[loop] == '\r')
			{
				/* If the previous character was a CR too, insert a newline */
				if (cr_received)
				{
					out_buffer[out_size] = '\n';
					out_size++;
				}
				cr_received = true;
			}
			else
			{
				if (str[loop] == '\n')
				{
					/* If we get a newline without a CR first, insert a CR */
					if (!cr_received)
					{
						out_buffer[out_size] = '\r';
						out_size++;
					}
				}
				else
				{
					/* If we receive a normal char, and the previous one was a
					   CR insert a newline */
					if (cr_received)
					{
						out_buffer[out_size] = '\n';
						out_size++;
					}
				}
				cr_received = false;
			}
			out_buffer[out_size] = str[loop];
			out_size++;
		}
		str = out_buffer;
//	size = out_size;
	}

	return true;
}

void Terminal::setAutoCrLf (bool option)
{
	autocrlf = option;
}
