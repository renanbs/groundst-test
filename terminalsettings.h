#ifndef TERMINALSETTINGS_H
#define TERMINALSETTINGS_H

#include <QDialog>
#include <QSettings>
#include "terminal.h"

class Terminal;
namespace Ui {
	class TerminalSettings;
}

class TerminalSettings : public QDialog
{
		Q_OBJECT

	public:
		explicit TerminalSettings (QWidget *parent = 0, Terminal *t = 0);
		virtual ~TerminalSettings ();
		Terminal *term;

	private slots:
		void on_buttonBox_accepted();

	private:
		Ui::TerminalSettings *ui;
		void loadConfiguration ();
		bool autocrlf;
};

#endif // TERMINALSETTINGS_H
