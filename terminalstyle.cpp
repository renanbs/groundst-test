#include "terminalstyle.h"
#include "ui_terminalstyle.h"
#include <QColorDialog>
#include <QFontDialog>
#include <QPalette>
#include <QSettings>


TerminalStyle::TerminalStyle(QWidget *parent, Terminal *t) : QDialog(parent), ui(new Ui::TerminalStyle)
{
	ui->setupUi(this);
	term = t;
	loadConfiguration ();
}

TerminalStyle::~TerminalStyle()
{
	delete ui;
}

void TerminalStyle::loadConfiguration ()
{
	pal = new QPalette();
	QFont font ("Courier New", 10);
	QColor backColor;
	QColor fontColor;
	QSettings settings ("groundST", "style");
	/// If exists at least one setting, testing for background color
	if (settings.contains ("backColor"))
	{
		backColor = settings.value ("backColor").value<QColor>();
		fontColor = settings.value ("fontColor").value<QColor>();
		font = settings.value ("font").value<QFont>();
	}
	else
	{
		backColor = Qt::black;
		fontColor = Qt::white;
	}

	pal->setColor(QPalette::Window, backColor);
	pal->setColor(QPalette::WindowText, fontColor);
	ui->labelExample->setPalette(*pal);
	ui->labelExample->setAutoFillBackground (true);
	ui->labelExample->setFont (font);
}

void TerminalStyle::on_pushButtonBackGround_clicked()
{
	QColor color = QColorDialog::getColor(Qt::black, this);
	if (color.isValid())
	{
		pal->setColor (QPalette::Window, color);
		ui->labelExample->setPalette(*pal);
		ui->labelExample->setAutoFillBackground(true);
	}
}

void TerminalStyle::on_pushButtonFontColor_clicked()
{
	QColor color = QColorDialog::getColor(Qt::white, this);
	if (color.isValid())
	{
		pal->setColor(QPalette::WindowText, color);
		ui->labelExample->setPalette(*pal);
		ui->labelExample->setAutoFillBackground(true);
	}
}

void TerminalStyle::on_pushButtonFontType_clicked()
{
	bool ok;
	QFont font = QFontDialog::getFont (&ok, QFont ("Courier New", 10), this);
	if (ok)
		ui->labelExample->setFont (font);
}

void TerminalStyle::on_buttonBox_accepted()
{
	QPalette p = term->palette();
	QColor backColor = pal->color(QPalette::Window);
	p.setColor(QPalette::Base, backColor);
	QColor fontColor = pal->color(QPalette::WindowText);
	p.setColor(QPalette::Text, fontColor);
	QFont font = ui->labelExample->font();
	term->setFont(font);
	term->setPalette(p);
	term->setAutoFillBackground (true);

	QSettings settings ("groundST", "style");
	settings.setValue ("backColor", backColor);
	settings.setValue ("fontColor", fontColor);
	settings.setValue ("font", font);
}

void TerminalStyle::applySettings ()
{
	QPalette p = term->palette();
	QColor backColor = pal->color(QPalette::Window);
	p.setColor(QPalette::Base, backColor);
	QColor fontColor = pal->color(QPalette::WindowText);
	p.setColor(QPalette::Text, fontColor);
	QFont font = ui->labelExample->font ();
	term->setFont (font);
	term->setPalette (p);
}

void TerminalStyle::on_pushButtonApply_clicked()
{
	applySettings ();
}

void TerminalStyle::on_pushButtonRestoreDefault_clicked()
{
	pal->setColor (QPalette::Window, Qt::black);
	pal->setColor (QPalette::WindowText, Qt::white);
	ui->labelExample->setPalette (*pal);
	ui->labelExample->setAutoFillBackground (true);

	QFont font ("Courier New", 10);
	ui->labelExample->setFont(font);
}

void TerminalStyle::on_buttonBox_rejected()
{
	loadConfiguration ();
	applySettings ();
}
