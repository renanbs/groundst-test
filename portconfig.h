#ifndef PORTCONFIG_H
#define PORTCONFIG_H

#include <QDialog>
#include <QSettings>
#include <QFile>
#include <QDir>
#include "qextserialenumerator.h"
#include "qextserialport.h"
#include "port.h"

class Port;

namespace Ui
{
	class PortConfig;
}

class PortConfig : public QDialog
{
		Q_OBJECT

	public:
		explicit PortConfig(QWidget *parent = 0, Port *p = 0);
		~PortConfig();

	private slots:
		void on_buttonBox_accepted();

	private:
		Port *uart;
		Ui::PortConfig *ui;

		int savePortConfig ();
		int loadPortConfig ();
};

#endif // PORTCONFIG_H
